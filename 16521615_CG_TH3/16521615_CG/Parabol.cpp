
#include "Parabol.h"
#include<math.h>
#include<SDL.h>
void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	float a = 1 / (2 * A), p = a - 1 / 2;;
	int x = 0, y = 0, w, h;
	Draw2Points(xc, yc, x, y, ren);
	while (x<A)
	{
		if (p > 0)
		{
			p = p - 2 * x - 1;
		}
		else
		{
			p = p + 2 * A - 1 - 2 * x;
			y += 1;
		}
		x += 1;
		Draw2Points(xc, yc, x, y, ren);
	}
	x = A, y = A / 2;
	SDL_GetRendererOutputSize(ren, &w, &h);
	while (x<w && y<h)
	{
		if (p<0)
		{
			p = p + 2 * sqrt(float(2 * A))*(sqrt(float(y + 1)) - sqrt(float(y)));
		}
		else
		{
			p = p + 2 * sqrt(float(2 * A))*(sqrt(float(y + 1)) - sqrt(float(y))) - 2;
			x = x + 1;
		}
		y = y + 1;
		Draw2Points(xc, yc, x, y, ren);
	}
}
void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	float a = 1 / (2 * A), p = a - 1 / 2;;
	int x = 0, y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while (x<A)
	{
		if (p > 0)
		{
			p = p - 2 * x - 1;
		}
		else
		{
			p = p + 2 * A - 1 - 2 * x;
			y++;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	x = A;
	y = A / 2;
	int w, h;
	SDL_GetRendererOutputSize(ren, &w, &h);
	while (x<w && y<h)
	{
		if (p<0)
		{
			p = p + 2 * sqrt(float(2 * A))*(sqrt(float(y + 1)) - sqrt(float(y)));
		}
		else
		{
			p = p + 2 * sqrt(float(2 * A))*(sqrt(float(y + 1)) - sqrt(float(y))) - 2;
			x = x + 1;
		}
		y = y + 1;
		Draw2Points(xc, yc, x, y, ren);
	}
}