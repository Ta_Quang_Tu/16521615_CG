#include "DrawPolygon.h"
#include <iostream>
#define PI 3.14159265
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];
	float phi = PI / 2;
	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi =phi + 2 * PI / 3;
	}
	DDA_Line(x[0] + xc, y[0] + yc, x[1] + xc, y[1] + yc, ren);
	DDA_Line(x[0] + xc, y[0] + yc, x[2] + xc, y[2] + yc, ren);
	DDA_Line(x[1] + xc, y[1] + yc, x[2] + xc, y[2] + yc, ren);

}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4], y[4];
	float phi = 3.14 / 2;
	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi = phi + PI / 2;
	}
	Midpoint_Line(x[0] + xc, y[0] + yc, x[1] + xc, y[1] + yc, ren);
	Midpoint_Line(x[1] + xc, y[2] + yc, x[2] + xc, y[2] + yc, ren);
	Midpoint_Line(x[3] + xc, y[3] + yc, x[2] + xc, y[2] + yc, ren);
	Midpoint_Line(x[3] + xc, y[3] + yc, x[0] + xc, y[0] + yc, ren);
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi = phi + 2 * PI / 5;
	}
	Midpoint_Line(x[0] + xc, y[0] + yc, x[1] + xc, y[1] + yc, ren);
	Midpoint_Line(x[1] + xc, y[1] + yc, x[2] + xc, y[2] + yc, ren);
	Midpoint_Line(x[3] + xc, y[3] + yc, x[2] + xc, y[2] + yc, ren);
	Midpoint_Line(x[3] + xc, y[3] + yc, x[4] + xc, y[4] + yc, ren);
	Midpoint_Line(x[0] + xc, y[0] + yc, x[4] + xc, y[4] + yc, ren);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6], y[6];
	float phi = PI / 2;
	for (int i = 0; i < 6; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi = phi + PI / 3;
	}
	Midpoint_Line(x[0] + xc, y[0] + yc, x[1] + xc, y[1] + yc, ren);
	Midpoint_Line(x[1] + xc, y[1] + yc, x[2] + xc, y[2] + yc, ren);
	Midpoint_Line(x[3] + xc, y[3] + yc, x[2] + xc, y[2] + yc, ren);
	Midpoint_Line(x[3] + xc, y[3] + yc, x[4] + xc, y[4] + yc, ren);
	Midpoint_Line(x[5] + xc, y[5] + yc, x[4] + xc, y[4] + yc, ren);
	Midpoint_Line(x[5] + xc, y[5] + yc, x[0] + xc, y[0] + yc, ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi = phi + 2 * PI / 5;
	}
	Midpoint_Line(x[0] + xc, y[0] + yc, x[2] + xc, y[2] + yc, ren);
	Midpoint_Line(x[0] + xc, y[0] + yc, x[3] + xc, y[3] + yc, ren);
	Midpoint_Line(x[1] + xc, y[1] + yc, x[3] + xc, y[3] + yc, ren);
	Midpoint_Line(x[1] + xc, y[1] + yc, x[4] + xc, y[4] + yc, ren);
	Midpoint_Line(x[2] + xc, y[2] + yc, x[4] + xc, y[4] + yc, ren);
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	int x1[5], y1[5];
	float t = (tan(3 * PI / 10) / tan(2 * PI / 5));
	int r = (R*(1 - t) / (1 + t));
	float phi = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}
	phi = 7 * PI / 10;
	for (int i = 0; i < 5; i++)
	{
		x1[i] = xc + int(r*cos(phi) + 0.5);
		y1[i] = yc - int(r*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}
	for (int i = 0; i < 5; i++)
	{
		Midpoint_Line(x[i], y[i], x1[i], y1[i], ren);
		Midpoint_Line(x1[i], y1[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}
//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], y[8];
	float phi = PI / 2;
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi = phi + PI / 4;
	}
	Midpoint_Line(x[0] + xc, y[0] + yc, x[3] + xc, y[3] + yc, ren);
	Midpoint_Line(x[0] + xc, y[0] + yc, x[5] + xc, y[5] + yc, ren);
	Midpoint_Line(x[1] + xc, y[1] + yc, x[4] + xc, y[4] + yc, ren);
	Midpoint_Line(x[1] + xc, y[1] + yc, x[6] + xc, y[6] + yc, ren);
	Midpoint_Line(x[2] + xc, y[2] + yc, x[5] + xc, y[5] + yc, ren);
	Midpoint_Line(x[2] + xc, y[2] + yc, x[7] + xc, y[7] + yc, ren);
	Midpoint_Line(x[3] + xc, y[3] + yc, x[7] + xc, y[7] + yc, ren);
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = PI / 2 + startAngle;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi = phi + 2 * PI / 5;
	}
	Midpoint_Line(x[0] + xc, y[0] + yc, x[2] + xc, y[2] + yc, ren);
	Midpoint_Line(x[0] + xc, y[0] + yc, x[3] + xc, y[3] + yc, ren);
	Midpoint_Line(x[1] + xc, y[1] + yc, x[3] + xc, y[3] + yc, ren);
	Midpoint_Line(x[1] + xc, y[1] + yc, x[4] + xc, y[4] + yc, ren);
	Midpoint_Line(x[2] + xc, y[2] + yc, x[4] + xc, y[4] + yc, ren);
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	int x[5], y[5];
	float t = (tan(3 * PI / 10) / tan(2 * PI / 5));
	float phi = PI / 2;
	while (r > 1){
		for (int i = 0; i < 5; i++)
		{
			x[i] = xc + int(r*cos(phi) + 0.5);
			y[i] = yc - int(r*sin(phi) + 0.5);
			phi += 2 * PI / 5;
		}
		for (int i = 0; i < 5; i++){
			Midpoint_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
		}
		r = (r*(1 - t) / (1 + t));
		phi = phi + PI / 5;
	}
}