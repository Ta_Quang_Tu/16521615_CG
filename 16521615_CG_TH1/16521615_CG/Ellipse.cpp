#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
}
void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int p, a2, b2;
	int x, y;
	a2 = a*a;
	b2 = b*b;
	x = 0;
	y = b;
	p = 2 * (b2)-(2 * b)*a2 + a2;
	//Area 1
	while ((b2)*x <= y*a2)
	{
		Draw4Points(yc, yc, x, y, ren);
		if (p<0)
		{
			p = p + 2 * (b2)*(2 * x + 3);
		}
		else{
			p = p - 4 * y*a2 + 2 * (b2)*(2 * x + 3);
			y--;
		}
		x++;
	}
	//Area 2
	y = 0;
	x = a;
	p = 2 * (a2)-2 * a*b2 + b2;
	while ((a2)*y <= x*b2)
	{
		Draw4Points(yc, yc, x, y, ren);
		if (p<0)
		{
			p = p + 2 * (a2)*(2 * y + 3);
		}
		else
		{
			p = p - 4 * x*b2 + 2 * (a2)*(2 * y + 3);
			x = x - 1;
		}
		y = y + 1;
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int x, y, fx, fy, a2, b2, p;
	x = 0;
	y = b;
	a2 = a*a;
	b2 = b*b;
	fx = 0;
	fy = 2 * a2 * y;
	Draw4Points(xc, yc, x, y, ren);
	//Area 1
	p = b2 - (a2*b) + (0.25*a2);
	while (fx<fy)
	{
		x++;
		fx += 2 * b2;
		if (p<0)
		{
			p += b2*(2 * x + 3);
		}
		else
		{
			y--;
			p += b2*(2 * x + 3) + a2*(2 - 2 * y);
			fy -= 2 * a2;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
	p = b2*(x + 0.5)*(x + 0.5) + a2*(y - 1)*(y - 1) - a2*b2;
	//Area 2
	while (y>0)
	{
		y--;
		fy -= 2 * a2;
		if (p >= 0)
		{
			p += a2*(3 - 2 * y);
		}
		else
		{
			x++;
			fx += 2 * b2;
			p += b2*(2 * x + 2) + a2*(3 - 2 * y);
		}
		Draw4Points(xc, yc, x, y, ren);
	}

}